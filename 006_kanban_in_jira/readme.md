# Understanding the Kanban board
For those that are familiar with the Scrum board in JIRA Agile, the Kanban board will look very similar to the work mode of the Scrum board, with only a few differences:

* There is no backlog, or rather the first column on your board is your backlog
* There are no active sprints or sprint planning
* Some of the columns may have a minimum and maximum number, which appears next to the column name
* Some of the columns may be highlighted in red or yellow, as shown in the following screenshot, where the In Progress column is highlighted in red:

![](assets/markdown-img-paste-20201109143337814.png)

So, let's take a closer look at the Kanban board and see why we have these differences. First of all, as explained in the earlier section, Kanban does not use discrete time periods such as iterations to plan work in advance. Instead, work is being done constantly, going from the backlog to the finish line to be released. Therefore, the Kanban board does not have separate views for the backlog (or Plan mode in older versions of JIRA Agile) and the current active sprints (or Work mode), everything is combined into this single Kanban board view, which shows you everything in your backlog and the current progress as your team works through them.

## Creating a new Kanban board
JIRA Agile provides a simple-to-follow wizard to help you create new Kanban boards. All you need to do is choose if you want to create a board from scratch, or from an existing project or filter, and follow the steps.

To create a new Kanban board, perform the following steps:

1. Click on the Agile menu from the top navigation bar and select the Manage Boards option.
1. Click on the Create board button. This will bring up the Create an Agile board dialog.
1. Select the Create a Kanban board option
![](assets/markdown-img-paste-2020110914344568.png)
1. Select how you want to create your board and click on the Next button. There are three options to choose from to create your board. These options are explained as follows:
  * New project and a new board: This is the same as creating a project using the Scrum Agile project template. A new project will be created along with a new Kanban board dedicated to the project.
  * Board from an existing project: This option allows you to create a new board from your existing projects. The board will be dedicated to only one project.
  * Board from an existing Saved Filter: This option allows you to create a board that can span across multiple projects with the use of a filter. So, to use this option, you will first have to create a filter that includes the projects and issues you need.
![](assets/markdown-img-paste-20201109143531339.png)
1. Fill in the required information for the board. Depending on the option you have selected, you will either need to provide the project details or select a filter to use. The following screenshot shows an example of creating a board with a filter. Click on the Create board button to finish:
![](assets/markdown-img-paste-20201109143546633.png)

### Customizing your Kanban board

After you have created your Kanban board, you will need to customize it based on your workflow process and your team's capacity. Some of the key customizations include:

* Modeling your board columns to mimic your workflow
* Setting minimum and maximum constraints for your workflow status so as to control the work in progress
* Controlling what issues will be included and displayed on the board
* Categorizing issues on the Kanban board in swimlanes
* Creating custom filtered views with quick filters

### Visualizing workflows with Kanban

One of the core concepts of Kanban is to visualize the team's workflow to better understand what the team is currently working on, what the workload is like, and where in the workflow a given task is.

In JIRA Agile, workflow statuses are represented as columns on the Kanban board, and you can customize your Kanban board's columns to closely mimic your actual workflow. To customize your Kanban board's columns, perform the following steps:

1. Go to the Kanban board you want to customize.
1. Click on the Board dropdown menu and select the Configure option.
1. Select Columns from the left navigation.
1. Click on the Add column button.
1. Enter a name for the new column and select a category for it. Generally speaking, if the column represents the start of the workflow, then it should be To Do. If it represents the end, then it should be Done. Otherwise, use In Progress.
1. Click on the Add button to create the new column:

![](assets/markdown-img-paste-20201109144121261.png)

Once you have created the new column, it will be added as the second last column on the list. You can rearrange that by dragging it left or right on the list, so it is in the correct position in your workflow.

Generally speaking, your board should reflect how work progresses through your workflow, so you should have a column for each major step that members of your team will be working on. For example, if you have these three steps in your workflow, In Development, Development Completed, and In Testing, you should have two columns, one for In Development and one for In Testing. The reason why you should not have a column for Development Completed is because nobody will be working on the issues that are in the step.

### Setting up column constraints
As stated earlier, one of the key differences between Scrum and Kanban is that Scrum limits the amount of work per iteration, and Kanban limits the amount of work per workflow status. To set up column constraints, perform the following steps:

1. Go to the Kanban board you want to customize.
1. Click on the Board dropdown menu and select the Configure option.
1. Select Columns from the left navigation.
1. Choose how you want column constraint to be calculated from the Column Constraint field. If you select the None option, column constraint will not be applied to this board.
1. Set the minimum and maximum constraint value for the status in its corresponding columns. For example, as shown in the following screenshot, for the Selected for Development column, the team should have a minimum of five issues, and no more than ten issues in the status:
![](assets/markdown-img-paste-20201109144218224.png)
Once you have set the minimum and maximum constraints for workflow statuses, the Kanban board will let you know if those constraints are violated. For example, as shown in the following screenshot, we only have one issue in the Selected for Development status, which has a minimum constraint of 5 issues, so the column is highlighted in yellow. We also have six issues in the In Progress status, which has a maximum constraint of five issues, so the column is highlighted in red:

![](assets/markdown-img-paste-20201109144253423.png)

When you start seeing these constraint violations, it means the team should start reassessing the current workload.

### Controlling what issues to show on the board

For Kanban board, there are two queries used to determine what issues will be displayed:

* Saved Filter: The saved filter includes issues that will be displayed on the Kanban board. The Filter Query field shows the JQL query of the saved filter. Depending on how the board is created, the saved filter will be either automatically created or selected from an existing filter list.
* Kanban board sub-filter: The sub-filter determines which issues from the saved filter will be considered unreleased. As we will see in the later section, Releasing a version in Kanban, once an issue is released as part of a new version, it is removed from the board.
* As shown in the following screenshot, Sample Kanban Board is using the Filter for Sample Kanban Project saved filter with the JQL query of project = "Sample Kanban Project" ORDER BY Rank ASC. This means that all issues created in Sample Kanban Project will be shown on the board. It is also using the JQL query of fixVersion in unreleasedVersions() OR fixVersion is EMPTY, which means issues that have an unreleased version as their fix versions, or do not have a value for the field, will be considered unreleased and displayed on the board:

![](assets/markdown-img-paste-20201109144339563.png)

You can change both the saved filter and sub-filter used for your board. If you want to simply use a different saved filter for another filter you have, perform the following steps:

1. Hover over and click on the saved filter's name. You will see it change to a select list.
1. Select the new saved filter from the list. If you do not see the filter you want, you can type in the filter's name and search for it. Note that you can only see and select filters that you have access to.

You can also change the JQL query of the saved filter currently in use:
1. Click on the Edit Filter Query link under Saved Filter. This will bring you to Issue Navigator, the interface where you create and edit filters in JIRA, as shown in the following screenshot:
![](assets/markdown-img-paste-20201109144433952.png)
1. Update your search filter criteria via the UI controls if you are in the Basic mode, or the JQL query directly if you are in the Advanced mode.
1. Click on the Save button at the top to update your filter.

As shown in the preceding example, we changed our filter's query to project = "Sample Kanban Project" and issuetype in (Bug, Story, Task) ORDER BY Rank ASC, which limits the issues to types of bugs, stories, and tasks only.

To update the board's sub-filter to customize how unreleased issues are determined, here are the steps:

1. Hover over and click on the Kanban board sub-filter's content. You will see it change to an editable text box as shown in the following screenshot.
1. Enter the new JQL query and click on the Update button, as follows:

![](assets/markdown-img-paste-20201109144522765.png)

As shown in the preceding example, we have changed the sub-filter's query to fixVersion in unreleasedVersions() OR fixVersion is EMPTY or resolution is EMPTY, to also include a check for issues that do not have a value for the Resolution field.

### Organizing your Kanban board with swimlanes
A swimlane is a useful way to group and organize your issues on your Kanban board. For example, you can use swimlanes to represent priority, and divide issues based on owners or types.

By default, when you first create a new Kanban board, you will have two swimlanes, but you can also create your own custom ones. For example, as shown in the following screenshot, we have three swimlanes: the Expedite and the Everything Else swimlanes are created along with your board, and the Bugs swimlane in the middle is a custom swimlane we have added:

![](assets/markdown-img-paste-20201109144541452.png)
To customize swimlanes for your Kanban board, perform the following steps:

1. Go to the Kanban board that you want to customize swimlanes for.
1. Click on the Board dropdown menu and select the Configure option.
1. Select Swimlanes from the left navigation.
1. Choose the criteria you want to base your swimlane on in the Base Swimlane on field.
1. Create a new swimlane by entering the name and description for the swimlane, and enter the JQL query if you are basing your swimlanes on queries.
1. Reorder your swimlanes by dragging them up and down the list:

![](assets/markdown-img-paste-2020110914383417.png)

As shown in the preceding screenshot, we have three swimlanes based on queries. The Expedite and Everything Else swimlanes were there by default when we created the board. We have a new Bugs swimlane that is using the JQL query of issuetype = Bug, so all issues of the Bug type will be categorized together.

Note that the order of swimlanes is important. The order will determine both the placement of each swimlane and the swimlane an issue will belong to. In this case, the Expedite swimlane is on the top, so this will reflect in the final Kanban board. If we have an issue that is of the Bug type and also a value of Highest for the priority field, it will be categorized into the Expedite swimlane rather than the Bugs swimlane because of the order.

The JQL option is the most flexible way of defining swimlanes, but you can also use some of the built-in options such as Assignee for simpler purposes. The next screenshot shows a Kanban board with four swimlanes, each showing the issues for a user, so you easily get an idea of how many issues are assigned to each user:

![](assets/markdown-img-paste-20201109143741846.png)

### Creating custom views with quick filters
By default, the Kanban board will display all the issues that are returned from the selected filter. However, there might be times you need to do additional filtering on top, to narrow down the list of issues further. This is where quick filters come in.

Quick filters let you define additional filtering based on JQL. This is like creating a custom view for the board to include only the issues that you are interested to see for the time being. To create new quick filters, perform the following steps:

1. Go to the Kanban board you want to add quick filters to.
1. Click on the Board dropdown menu and select the Configure option.
1. Select Quick Filters from the left navigation.
1. Enter a name and description for the new quick filter.
1. Enter the JQL query for the filter. JIRA Agile will help you construct and validate the query.
1. Click on the Add button to create the new quick filter:

![](assets/markdown-img-paste-20201109143651163.png)

As shown in the preceding screenshot, we have three quick filters already created, and we are creating a new one named Exclude Duplicated with the JQL query of resolution not in (Duplicate), to remove all issues with the value of Duplicate in the resolution field from the board.

Once you have created the new quick filters, they will be displayed above the issue cards, as shown in the following screenshot:

![](assets/markdown-img-paste-20201109143627568.png)
