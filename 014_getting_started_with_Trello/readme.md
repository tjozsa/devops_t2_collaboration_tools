# Trello Basics

[[_TOC_]]

## Getting Started With Trello
Getting set up and running with a new app is no small task, so we’ll guide you through Trello onboarding with speed and simplicity in mind. You’ll have your team collaborating in Trello in no time!

## Board Basics
A Trello board only has four key components, but comes with unlimited possibility:

![](assets/markdown-img-paste-20201110081806777.png)

* **Boards** - A board represents a project or a place to keep track of information. Whether you are launching a new website, or planning a vacation, a Trello board is the place to organize your tasks and collaborate with your colleagues, family, or friends.

* **Lists** - Lists keep cards (C) organized in their various stages of progress. They can be used to create a workflow where cards are moved across lists from start to finish, or simply act as a place to keep track of ideas and information. There’s no limit to the number of lists you can add to a board, and they can be arranged however you like.

![](assets/markdown-img-paste-20201110081836761.png)

A basic (but effective) list setup for a board might be simply To Do, Doing, and Done, where cards start in the To Do list and make their way to the Done list. But don’t forget: Trello is truly customizable to your unique needs, so you can name your lists anything you like! Whether it’s basic Kanban, a sales pipeline, a marketing calendar, or project management, what matters most is establishing a workflow for the way your team works.

![](assets/markdown-img-paste-20201110081849102.png)

* **Cards** - The fundamental unit of a board is a card. Cards are used to represent tasks and ideas. A card can be something that needs to get done, like a blog post to be written, or something that needs to be remembered, like company vacation policies. Just click “Add a card…” at the bottom of any list to create a new card, and give it a name like “Pick up the dry cleaning” or “Write a blog post.”

Cards can be customized to hold a wide variety of useful information by clicking on them. Drag and drop cards across lists to show progress. There’s no limit to the number of cards you can add to a board.

![](assets/markdown-img-paste-20201110081901793.png)

* **Menu** - On the right side of your Trello board is the menu—the mission control center for your board. The menu is where you manage members, control settings, filter cards, and enable Power-Ups. You can also see all of the activity that has taken place on a board in the menu’s activity feed. Take some time to check out everything the menu has to offer.

Sounds pretty simple, right? Let’s get started by creating your first board, but first, think of a project or goal to work on. Need some ideas? Here’s a little [Inspiration](https://trello.com/inspiration).

## Create Your First Boards
Boards are where projects get organized, information is shared, and great work happens. They give everyone a shared perspective on the work getting done and what still needs to get done. Boards are made up of lists and cards. Lists often represent a workflow or process. Cards, which often represent tasks, move across these lists to completion.

Boards are where projects get organized, information is shared, and great work happens. They give everyone a shared perspective on the work getting done and what still needs to get done. Boards are made up of lists and cards. Lists often represent a workflow or process. Cards, which often represent tasks, move across these lists to completion.

![](assets/markdown-img-paste-20201110082011454.png)

Creating a great board is easy! Let's break it down:

### Create A Board
![](assets/markdown-img-paste-20201110082027686.png)
1. From the team’s Boards tab, click “Create new board” or click the plus button (+) in the Trello header directly to the left of your name, and select “Create Board.”
1. Name the board for whatever you are working on. (Remember: It can be anything from organizing an event or managing a blog, to tracking sales leads or planning that much needed vacation.)

>Check out our [Inspiration](https://trello.com/inspiration) page and [Team Playbooks](https://trello.com/teams) for board workflow ideas and examples.

### Add Lists
![](assets/markdown-img-paste-20201110082140304.png)

1. Click "Add a list" to add your first list to your board, such as "To Do." List names can be as simple as steps like “To Do,” “Doing,” and “Done” or as detailed as needed for the work you are doing.
1. Add as many lists to your board as you need to build out a workflow.

> Experiment with additional lists on your board, or for using lists as [repositories to hold information](https://trello.com/b/HbTEX5hb/employee-manual). Trello is infinitely flexible, so you can customize lists to the workflow that fits your team best.

### Add Cards
![](assets/markdown-img-paste-20201110082229883.png)
1. Add cards for each task that needs to be completed by clicking “Add A Card” in the first list. Keep card titles short to make it easier to scan and see the status of each card on the board.
1. So that everyone has a clear understanding of what needs to get done, click on cards to add more information such as:
* Due dates
* Descriptions
* Checklists
* Attachments
* Comments

>Easily add cards to a board in bulk from a list or spreadsheet by copying the list and pasting it into a new Trello card. Trello will automatically turn each line-separated item into a new card, and you can kiss those old tools goodbye.

![](assets/markdown-img-paste-20201110082302528.png)

### Invite Members
![](assets/markdown-img-paste-20201110082317465.png)

Invite members to your board so that they can be assigned to tasks and collaborate on your board. Click “Invite” in the board’s menu and select members of your team to add to your board, or invite members by their email address or name.

Get an easy-to-share invite link to your board at the bottom of the Invite menu. Drop the special link in a chat room or email and anyone with the link will be able to join your board and start collaborating.

>Add members to cards by dragging their avatar from the Members section of the menu onto Trello cards so that everyone knows what to do when they open the board.

### A Note On Board Privacy & Visibility:
![](assets/markdown-img-paste-2020111008234154.png)

1. Trello boards have three privacy settings: Private, Team Visible, and Public:
||Who can see it?|	Great for...|
|Private|	People you invite|	for HR boards with sensitive information|
|Team Visible|	Members of your team|	for collaborating with colleagues|
|Public|	Anyone|	for product roadmaps or volunteer groups|
1. Change the visibility of a board by clicking the current visibility status to the right of a board’s name.
1. Boards in teams make it easier for everyone you work with to collaborate together. For boards that shouldn’t be in a team at all, click the team name, then "Change Team," and set the board’s team to None.

Way to go! Your first board is all setup and ready for action.

## Feature Deep Dive

Trello might be simple on the surface, but there's endless power under the hood. Here's a deep dive into the Trello card back and board menu. They are two important items that are worth getting to know.

### The Card Back
Let’s click on a card to flip it around and take a closer look. This is the “card back” and it has three main sections worth getting to know:

![](assets/markdown-img-paste-20201110082604737.png)

1. **Card Descriptions** - In the description field you can add more specific information about your card, links to websites, or step by step directions. To add details to your card click “Edit the description” at the top of the card back. You can even format your text with Markdown.

![](assets/markdown-img-paste-20201110082628968.png)

1. **Comments and Activity** - Comments can be added to cards when communicating and collaborating with team members, like giving feedback or updates. @ mention a member of your board or team in a comment and they will receive a notification in Trello. The activity feed is a timeline of all of the comments and actions on a card.

![](assets/markdown-img-paste-20201110082650535.png)

1. **Add** - The “Add” section provides you with more tools for the back of a card.
  * Add Members to cards to assign people to tasks, and easily see who is doing what and what still needs to get done.
  * Add Checklists for cards that require subtasks or have multiple steps to make sure nothing falls through the cracks. You can even copy checklists from other cards on the board, and assign people to checklist items by @ mentioning them.
  * Add a Due date to cards with deadlines, and card members will receive a notification 24 hours before it’s due. Once the tasks are completed, due dates can be marked as done.
  * Add Attachments from both your computer and many cloud storage services like Dropbox, Google Drive, Box, and OneDrive.

### It's On The Menu

On the right side of your Trello board is the menu, the mission control center for your board. The menu is where you can manage members, control settings, and enable Power-Ups. Let’s look at some of the menu’s tastiest items.

![](assets/markdown-img-paste-20201110082743375.png)

**Board Members** - Invite, remove, and manage board membership permissions. Click “Invite” to add people to a board, and invite them by their email or Trello username. Team members are automatically surfaced to save you some clicks.

![](assets/markdown-img-paste-20201110082759844.png)

* Remove members from a board by clicking their avatar in the members section and selecting “Remove from Board.” Click your own avatar to leave a board if you’ve had enough of the productivity party. By default any board member can add and remove members to make collaboration easier, but admins can change this in the board settings.
* Manage board member permissions by clicking an avatar and selecting “Change permissions.” Only board admins can change board member permissions. We recommend having more than one admin on a board in case an admin is unavailable and important admin level settings need to be changed.

**Change Background** - Add some personality to your boards!

Your Trello board comes with a default Trello blue background, but just click “Change Background” and voila! There are additional colors and thousands of beautiful photos from which to choose. Give each board a unique feel. Who says productivity shouldn’t be fun, too?

**Power-Ups** - Power-Ups bring additional functionality and integrations to your Trello boards. Connect the other apps you rely on to your boards, so all information is visible in one place. Here are a few examples:

**Calendar**: See due dates in a calendar view, and bring a different perspective to a board with the Calendar Power-Up.

![](assets/markdown-img-paste-20201110082858167.png)

**Custom Fields**: Add more structured information like costs, time estimates, phone numbers, and more to cards. These items can be visible on both the front and back of the card.

![](assets/markdown-img-paste-20201110082922900.png)

These are just a couple Power-Ups Trello has to offer, see the full list [here](https://trello.com/power-ups) and check out the next article for a Power-Up and integrations deep dive. Every board comes with one free Power-Up. Business Class teams and Enterprise organizations get unlimited Power-Ups on their boards.

The menu has so much more to offer, so take a few minutes to dig around. You never know what kind of delight you might find.
