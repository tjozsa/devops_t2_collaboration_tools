# Discuss Current Communication tools
We start by discussing what tools people use to communicate:
* email
* spoken word (direct chain of command via meetings, rumors, legends, tales and what not)
* "interment" - telepathic capabilities involved

# What Communication Should look like in an IT organization?
Now let's imagine a perfect world, where...
... every important information is:
* written down
* accessible to everyone whom it may concern
* transparent on all levels

The way to get there in IT is by introduction of development methodologies.
* agile
* scrum
* Kanban
* etc.
