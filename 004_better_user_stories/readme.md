# User Story Best Practice
## Version (theme) vs Epic vs user story vs task
Agile development uses four clear delivery methods to bring structure to any project: **versions**, **sprints**, **epics** and **user stories**.
  * A **version** is a set of features and fixes released together as a single update to your product. Assigning issues to versions helps you plan the order in which new features (stories) for your product will be released to your customers. A version can be made up of multiple sprints
  * A **sprint** is a set period of time during which specific work has to be completed and made ready for review
  * An **epic** captures a large body of work. It is essentially a large user story that can be broken down into a number of smaller stories. It may take several sprints to complete an epic.
  * A **user story** (represented as an issue in Jira) captures a description of a feature from an end-user perspective. The user story describes the type of user, what they want and why. A user story helps to create a simplified description of a requirement. User stories are broken up into individual tasks that make up the story (represented as sub-tasks in Jira).

## Why use user stories?
* Keep yourself expressing business value
* Avoid introducing detail too early that would prevent design options and inappropriately lock developers into one solution
* Avoid the appearance of false completeness and clarity
* Get to small enough chunks that invite negotiation and movement in the backlog
* Leave the technical functions to the architect, developers, testers, and so on

## Creating a user story
The goal is to outline a particular value to the customer that a team can deliver in an iteration using a few short sentences, ideally using non-technical language. While traditional requirements (like use cases) try to be as detailed as possible, a user story is defined incrementally, in three stages:
  * The brief description of the need
  * The conversations that happen during backlog grooming and iteration planning to solidify the details
  * The tests that confirm the story's satisfactory completion

##  What makes a great user story
  * They don't go into detailed requirements.
  * User stories must be written using the following template:


  > As a [type of user], I want [goal] so that I [receive benefit].

  > **End user goal:** _______________________________
  
  > **End business goal:** _____________________________
  
  > **Acceptance criteria:**


  * product defines any initial functionality requirements they can think of
  * this is a loose outline that will be finalized through wireframes, flows and discussions
  * acceptance criteria must be finalized before dev starts


> **Measurement of success:** ___________________________

  * Measurement of success is the way in which we will prove the story was successful in meeting the user and business goal defined above. This will initially be left blank and will be determined through conversations with research, ux and product. This must be finalized before dev starts.

## Example user story
  > As a purchaser on the website I want the ability to pay with a credit card, so that I may immediately confirm my purchase.
  > **End user goal:** Purchase items with a credit card
  > **End business goal:** Increase purchases
  > **Acceptance criteria:**
    * Accept Discover, Visa, MC
    * Validate credit card number
    * Validate expiration data and CVV
    * Validate billing address
    * Generate success/failure messages after processing
  > **Measurement of success:** ___________________________

> “Stories are deliberately not fleshed out in detail until they are ready to be developed, you only need enough understanding to allow prioritization with other stories.” - Kent Beck

* A well-formed user story will follow the INVEST mnemonic.
  * I – Independent
    * Stories are easiest to work with if they are independent. That is, we'd like them to not overlap in concept, and we'd like to be able to schedule and implement them in any order.
  * N – Negotiable
    * A good story is negotiable. It is not an explicit contract for features; rather, details will be co-created by the customer and programmer during development. A good story captures the essence, not the details. Over time, the card may acquire notes, test ideas, and so on, but we don't need these to prioritize or schedule stories.
  * V – Valuable
    * A story needs to be valuable. We don't care about value to just anybody; it needs to be valuable to the customer. Developers may have (legitimate) concerns, but these framed in a way that makes the customer perceive them as important. This is especially an issue when splitting stories.
    * Think of a whole story as a multi-layer cake, e.g., a network layer, a persistence layer, a logic layer, and a presentation layer. When we split a story, we're serving up only part of that cake. We want to give the customer the essence of the whole cake, and the best way is to slice vertically through the layers. Developers often have an inclination to work on only one layer at a time (and get it "right"); but a full database layer (for example) has little value to the customer if there's no presentation layer.
  * E – Estimable
    * A good story can be estimated. We don't need an exact estimate, but just enough to help the customer rank and schedule the story's implementation. Being estimable is partly a function of being negotiated, as it's hard to estimate a story we don't understand. It is also a function of size: bigger stories are harder to estimate.
    * Finally, it's a function of the team: what's easy to estimate will vary depending on the team's experience. (Sometimes a team may have to split a story into a (time-boxed) "spike" that will give the team enough information to make a decent estimate, and the rest of the story that will actually implement the desired feature.)
  * S – Small
    * Good stories tend to be small. Stories typically represent at most a few person-weeks worth of work. (Some teams restrict them to a few person-days of work.) Above this size, and it seems to be too hard to know what's in the story's scope. Saying, "it would take me more than a month" often implicitly adds, "as I don't understand what-all it would entail." Smaller stories tend to get more accurate estimates.
  * T – Testable
    * A good story is testable. Writing a story card carries an implicit promise: "I understand what I want well enough that I could write a test for it." Several teams have reported that by requiring customer tests before implementing a story, the team is more productive.
    * "Testability" has always been a characteristic of good requirements; actually writing the tests early helps us know whether this goal is met. If a customer doesn't know how to test something, this may indicate that the story isn't clear enough, or that it doesn't reflect something valuable to them, or that the customer just needs help in testing. A team can treat non-functional requirements (such as performance and usability) as things that need to be tested. Figure out how to operationalize these tests will help the team learn the true needs.

* User story outline
  * User Story
  * End user goal
  * End business goal
  * Acceptance Criteria /this isn't finalized until the story is ready for development/
  * Measurement of Success /this is added after conversations with product, ux and research/

## User story dos and don’ts
* DO focus on a specific user
  * Avoid the generic role of User when writing user stories. User stories are about all of the role who interact with the system or who realize some value or benefit from the system.
* DO provide acceptance criteria
  * The product owner should list as many acceptance criteria as possible in order to clarify the intent of the story. Acceptance criteria will become QA test cases
* DO have conversations
  * update acceptance criteria and user story details based on your conversations
* DONT go into detailed requirements
* DONT provide the solution
  * User stories must be focused on the user need and benefit not solution

## Advantages of Jira; tips and tricks
  * Type `c` for shortcut to creating an issue
  * Add issue to epic - via backlog agile board or when viewing story
  * take advantage of Jira's text formatting for easy to follow descriptions

# Identifying and Improving Bad User Stories
[link to original blog post](https://www.agileconnection.com/article/identifying-and-improving-bad-user-stories)

> Summary: A written user story is a very short narrative—a sentence or two—describing some small piece of functionality that has business value. User stories are intended to foster collaboration and communication, but writing these short narratives poorly can negate agile’s flexibility. Charles Suscheck and Andrew Fuqua explain some common failure patterns that will help you focus on the right role, value, and business functionality when writing stories.

## Common Problems
### Excessive ‘So That’ (Misplaced Requirement)
The team typically expects details of a story to be found before the conjunction “so that,” which is used to explain the story’s value. When the product owner writes requirements in the “so that” section, it is easy to miss part of the real requirement since it’s hidden after “so that.” You can tell if there is a problem when the “so that” section is complex or has multiple parts. In this case, the true story may be so big that it can’t get done in a sprint.

> **Example**: As a Manny’s food service customer I need to save my list so that later I can save a copy, print, or email the list for other uses.

**Problem**: A team presented with this story may think, “Save a list? Okay, sounds pretty simple.” But the value statement infers otherwise.  If the ability to copy, print, and email a list is what the product owner really wants, it would be too big—a compound user story made up of many pieces. Because of the confusion, a programmer or tester is likely to miss that the product owner wanted printing and emailing, not just saving, to be done as part of the story.

> **Improvement**: As a Manny’s food service customer, I need to save, copy, print, and email my list so that I can edit it again, check a received shipment against a printed list, and send the list to a restaurant.

The requirement is no longer misplaced, but the story is now too big and needs to be broken down into a copy, print, and email type of story.

### Odyssey (Ultra-Huge Story)
If team members can’t even estimate a story at a gross level, they may take on too much work in the iteration and not get to done. An Odyssey is beyond an epic. It’s something compounded or diffused to the point of having no discernable value (which an epic has). Such a story will lead to long conversations with the product owner, or even a failure to get to done. The team’s velocity will be unpredictable, and team members might be frustrated with a constantly evolving story.
Example: As a Manny’s food service customer, I need to save, copy, print, and email my list so that I can edit it again, check a received shipment against a printed list, and send the list to a restaurant.

**Problem**: This example shows a very large story that is hard to estimate and hard to implement in a sprint. It’s difficult to see the value that holds this story (or epic) together. Stories containing “and” or “or” are likely candidates for splitting into several smaller stories.

**Improvement**: We need to split the large story into multiple stories:

> As a Manny’s food service customer, I need to save my list so that I can reorder from the list to create more accurate food orders.

> As a Manny’s food service customer, I need to copy my list so that I can use it as a starting point for creating another list.

> As a Manny’s food service customer, I need to print my list so that I can check a received shipment against the printed list.

> As a Manny’s food service customer, I need to email my list so that I can have someone who doesn’t use the system review my list.

### Waterfall
Stories that contain only analysis, design, or technical aspects lead to waterfall development in two-week phases.

> **Example**: As a developer, I want to finalize the database table changes and additions for the release so that we don’t have to make changes to the model later.

**Problem**: This story has no business value and a user (“developer”) who is not really a system end user.  There is only the technical side of the equation. This story will likely lead to coding with a separate testing story.

> **Improvement**: Remove this developer story because it’s not really a user story. Instead, evolve the database as part of other stories. Develop the software in thin, vertical slices rather than horizontal architectural layers. Always provide business value in every story.


### Rigidity (Inflexible)
Stories with too much detail are often inflexible, leaving little room for creativity, better solutions, or dynamic scope control during development. Avoid this by postponing decisions on details that constrain the solution or that specify an implementation. Defer these decisions to a later but still responsible moment in order to maintain maximum flexibility.  The product owner is likely to get exactly what he asks for instead of what he really needs.

> **Example**: As a Manny’s food service customer, I want to see different food item types displayed in different colors—RGB = #FF0000 for meats, #A52AFA for grains, and #808000 for vegetables and fruits—so that I can quickly identify my food items by food type.

**Problem**: There may be better solutions that leave more flexibility, like using general colors or using intent of the colors rather than the color themselves. Specifying this level of technical detail handcuffs the programmers and possibly limits innovation.

> **Improvement**: As a Manny’s food service customer, I want my custom item code to stand out so that I can find it on the screen more quickly.

### For Whom? (Non-User)
There are a lot of different types of non-user stories, like using a specific name, a role, or the system. It’s better to write user stories for the role that actually wants the benefit or value provided by the user story.

> **Example**: As a the marketing manager, I would like logins to time out and log off after a preset number of seconds in case users leave their computers unattended.

**Problem**: A marketing manager may be a meaningless role if marketing uses the system exactly the same as advertising. Perhaps departments have administrators, super users, and casual users who use the system differently. In this case it may be better to have a role of administrator, super user, or casual user.

> **Improvement**: As a customer who is logged in, I would like my login to time out and log off after a preset number of seconds so that I can leave my computer unattended and still have some measure of protection against unauthorized use.

At the other extreme is the most overused and overly vague persona: “As a user” and its variants. Knowing the specific role for a story helps us understand the context for the story, leading to better value and focus.

> **Example**: As a business user, I would like a report of item profitability so that I can identify and highlight profitable items and consider what to do about underperforming items.

**Problem**: Who is this vague user? Is there a specific role that is interested in this report? Understanding that this story is for a specific role enables conversations about how to limit scope. In the previous example, “marketing manager” wasn’t appropriate; sometimes it is.

> **Improvement**: As a marketing manager considering how to spend limited marketing dollars, I need a report of the most/least profitable items so that I can identify and highlight profitable items and consider what to do about underperforming items.

Another non-user is “the system.” Beginning the user story with the phrase “As the system” may enable teams to use a waterfall approach. The system doesn’t care if business value is delivered or not. With the waterfall approach, no business value is delivered until the end.

> **Example**: As the system, I need to store customer account info and their order lists in the database.

**Problem**: This story doesn’t deliver any value to the end user. Manny’s users cannot directly use the database.

> **Improvement**: These aren’t good stories by themselves. For most applications, avoid creating stories just for database work. Add the database capabilities as a task to other user stories. Good user stories are thin, vertical slices through the application and provide business value.

### Parakeet Value
Beware when the story’s “so that” phrase is a restatement of the story’s “I want” phrase. This points to a lack of analysis depth. The real goal of the story is not at all clear, making it easy to be off target and develop software that doesn’t maximize business value. Such stories are often not analyzed well enough.

> **Example**: As a customer ordering food, I want to locate previous food order lists so that I can see all the lists that I have.

**Problem**: This story doesn’t explain its value.  In the case of Manny’s, why does the customer ordering food need to see lists? The answer could influence the usability.

> **Improvement**: As a customer ordering food, I want to see my saved food order lists so that I can reuse the list for future orders, making ordering faster and more accurate.

### Techie Value
Beware of user stories where “so that” is a technical capability, not any value to the end user. This is very similar to no business value, but the value listed is technical.

> **Example**: As a tester, I want to have detailed test plans so that when the system is completed, I can test the system.

**Problem**: This story refers to a specific user and has a technical benefit: the existence of test plans. Users want quality software, but they don’t care about test plans

> **Improvement**: The improvement would be to delete this story. Part of the team’s test plan should be found in the acceptance criteria for each story.
