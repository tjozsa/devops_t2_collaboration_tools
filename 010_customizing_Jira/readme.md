# Customizing Jira

* Create your own issue types and make them available to your projects
* Add new custom fields to collect more relevant data from users
* Customize workflows to better mimic your processes
* Secure your Jira with different levels of permission control
* Manage email notifications sent out from Jira to notify users of changes to issues

[[_TOC_]]

## Customizing issue types
Each issue has a type (therefore, the name "issue type"), which is represented by the issue type field. This lets you know what type of issue it is; for example, if the issue is a bug or a feature. Jira comes with a list of pre-defined issue types, such as Story, Epic, and Task.

The default issue types are great for simple software development projects, but they do not necessarily meet the needs of others. Since it is impossible to create a system that can address everyone's needs, Jira lets you create your own issue types and assigns them to projects. For example, for a help desk project, you might want to create a custom issue type called ticket. You can create this custom issue type and assign it to the Help Desk project and users will be able to log tickets, instead of bugs, in the system as shown here:

1. Log in to Jira as a Jira administrator.
1. Browse to the Jira administration console.
1. Select the Issues tab and then the Issue types option:
![](assets/markdown-img-paste-20201109195828818.png)
1. Click on the Add issue type button.
1. Enter the name and description for the new issue type.
1. Select whether the new issue type will be a Standard Issue Type or a Sub-Task Issue Type:
![](assets/markdown-img-paste-20201109195903680.png)
1. Click on Add to create the new issue type.

After you have created your new issue type, you will need to make it available to the project. You can do this by adding the new issue type to the issue type scheme used by the project. An issue type scheme contains a list of issue types, and is applied to one or more projects. This way, a project can have its own set of issue types, or have a shared set of issue types with other similar projects. To add your new issue type to an issue type scheme:

1. Browse to the Jira administration console.
1. Select the Issues tab and then the Issue types schemes option.
1. Look for the issue type scheme used by the project and click its Edit link.
1. Drag and drop the new issue type from the right Available Issue Types column to the left Issue Types for Current Scheme column.
1. Click the Save button:
![](assets/markdown-img-paste-20201109195950604.png)

## Adding new custom fields
Just like issue types, Jira comes with a number of built-in fields. You have already seen a few of them in the previous chapters. For example, when creating new stories for your agile boards, there are fields such as summary, priority, and assignee. These fields make up the backbone of an issue, and you cannot remove them from the system. For this reason, they are referred to as system fields.

While Jira's built-in fields are quite comprehensive for agile software development uses, most organizations soon find they have special requirements that cannot be addressed simply with the default fields available. To help you tailor Jira to your organization's needs, Jira lets you create and add your own fields to the system, called custom fields.

Jira comes with many types of custom fields, ranging from simple text fields, and select lists, to more complex ones, such as cascading select lists and user selectors. And if you find these are not sufficient enough, there are many more you can get from the Atlassian Marketplace, where you can find more specialized fields that talk to Salesforce.com, and many more. To start adding new custom fields to Jira:

1. Browse to the Jira administration console.
1. Select the Issues tab and then the Custom fields option.
1. Click on the Add Custom Field button. This will bring you to step 1 of the process, where you can select the custom field type.
1. Search and select the custom field type you wish to add, and click on Next. This will bring you to step 2 of the process, where you can specify the custom field's name and options:
![](assets/markdown-img-paste-20201109200105387.png)*If you do not see the field type you are looking for, select the All option from the left-hand side and then search again.*
1. Enter values for the Name and Description fields. If you are creating a selection-based custom field, such as a select list, you will need to add its select options too (you can update this list later):
![](assets/markdown-img-paste-20201109200153744.png)
1. Click on the Create button. This will bring you to the last step of the process, where you can specify the screen to which you would like to add the field.
1. Select the screens and click on Update. The following screenshot shows that the newly created field has been added to both of the support screens:
![](assets/markdown-img-paste-2020110920021181.png)

After you have created the custom field, you can always come back later and customize it. For example, with our origin select list field, we have added three options when creating the field. If more options are needed, we can edit the field and add more options to it.

## Customizing Jira workflows
Up until now, all the Scrum and Kanban projects we have created have their own workflow, named Software Simplified Workflow for Project X, and this is what is called the Simplified Workflow. A simplified workflow is a type of workflow that is designed specifically to work with agile projects, by having minimum restrictions. This means:

* There is a pre-defined set of statuses/columns for both Scrum and Kanban
* Board administrators can easily make changes, by simply adding and removing columns
* Issues can be moved between the statuses/columns freely with no restrictions
* There are no intermediate screens when an issue is being moved

This is great for most standard agile projects, where the goal is to have a straightforward workflow, and freedom for team members to move issues on the board as needed. However, sometimes you will have special requirements around your processes and your workflow will need to reflect those. For example, you might have a validation requirement that an issue can only be closed if a quality assurance engineer has signed off on it, or you might have a security requirement that only members of a certain group can move an issue to a target status.

These requirements may seem counter-intuitive in an agile environment, but remember that nothing is a one size fits all solution, and Jira lets you customize the workflow used by your project to best fulfill all your needs.

Jira comes with a very intuitive graphical tool to help you, as a Jira administrator, to create and customize your workflows. The following is a screenshot of how Jira visually represents a workflow in the tool:

![](assets/markdown-img-paste-20201109200302720.png)

Each status is represented as a box. Each transition is represented as a line between two statuses. When clicking on either a status or a transition, a properties panel will appear on the right-hand side, allowing you to further customize it.

## Authoring a workflow

So, let's take a look at how to create and set up a new workflow in Jira. To create a new workflow, all you need is a name and description:

1. Browse to the Jira administration console.
1. Select the Issues tab and then the Workflows option.
1. Click on the Add Workflow button.
1. Enter a name and description for the new workflow in the Add Workflow dialog.
1. Click on the Add button to create the workflow.

The newly created workflow will only contain the default create and open status, so you will need to configure it by adding new statuses and transitions to make it useful. Let's start with adding new statuses to the workflow using the following steps:

1. Click on the Add status button.
1. Select an existing status from the drop-down list. If the status you need does not exist, you can create a new status by entering its name and pressing the Enter key on your keyboard.
1. Check the Allow all statuses to transition to this one option, if you want users to be able to move the issue into this status regardless of its current status. This will create a Global Transition, which is a convenient way to allow an issue to use this transition from any status to get to the target status.
1. Click on the Add button to add the status to your workflow. You can repeat these steps to add as many statuses as you want to your workflow:
![](assets/markdown-img-paste-20201109200418641.png)
*Try to re-use existing statuses if possible, so you do not end up with many similar statuses to manage.*

Now that the statuses are added to the workflow, they need to be linked with transitions, so issues can move from one status to the next. There are two ways to create a transition:
* Click on the Add transition button or you can follow the next step
* Select the originating status, then click and drag the arrow to the destination status

Both options will bring up the Add transition dialog, as shown in the following screenshot:
![](assets/markdown-img-paste-20201109200501454.png)

From the preceding screenshot, you can choose to either create a new transition with the New Transition tab or use an existing transition with the Reuse a transition tab.

When creating a new transition, you will need to configure the following:
* From status: The originating status. The transition will be available when the issue is in the selected status.
* To status: The destination status. Once the transition is executed, the issue will be put into the selected status.
* Name: Name of the transition. This is the text that will be displayed to users. It is usually a good idea to name your transitions starting with a verb, such as Close Issue or Submit for Review.
* Description: An optional text description is the purpose of this transition. This will not be displayed to users.
* Screen: An optional intermediate screen to be displayed when users execute the transition. For example, you display a screen to capture additional data as part of the transition. If you do not select a screen, the transition will be executed immediately. The following screenshot shows a workflow screen:
![](assets/markdown-img-paste-20201109200535597.png)
If you want to reuse an existing transition, simply select the Reuse a transition tab, the From status and To status, and the transition to reuse, as shown in the following screenshot:
![](assets/markdown-img-paste-2020110920054745.png)
*Jira will only list valid transitions based on the To status selection.*

You might be wondering when you should create a new transition and when you should reuse an existing transition. The big difference between the two is that when you reuse a transition, all instances of the reused transition, also known as common transition, will share the same set of configurations, such as conditions and validators. Also, any changes made to the transition will be applied to all instances. A good use case for this is when you need to have multiple transitions with the same name and setup, such as Close Issue; instead of creating separate transitions each time, you can create one transition and reuse it whenever you need a transition to close an issue. Later on, if you need to add a new validator to the transition to validate additional user input, you will only need to make the change once, rather than multiple times for each Close Issue transition.

Another good practice to keep in mind is to not have a dead end state in your workflow, for example, allowing closed issues to be reopened. This will prevent users from accidentally closing an issue and not being able to correct the mistake.

Now that we have seen how to add new statuses and transitions to a workflow, let's look at adding conditions, validators, and post functions to a transition.

## Adding a condition to transitions
New transitions do not have any conditions by default. This means that anyone who has access to the issue will be able to execute the transition. By adding a condition to a workflow transition, you can control who or when an issue can be transitioned.

*Conditions take place before a transition is executed.*

Jira allows you to add any number of conditions to the transition:

1. Select the transition you want to add conditions to.
1. Click on the Conditions link.
1. Click on the Add condition link. This will bring you to the Add Condition To transition page, which lists all the available conditions you can add.
1. Select the condition you want to add.
1. Click on the Add button to add the condition.
1. Depending on the condition, you may be presented with the Add Parameters To Condition page, where you can specify the configuration options for the condition. For example, the User Is In Group condition will ask you to select the group to check against.

Newly added conditions are appended to the end of the existing list of conditions, creating a condition group. By default, when there is more than one condition, a logical AND is used to group the conditions. This means that all conditions must pass for the entire condition group to pass. If one condition fails, the entire group fails, and the user will not be able to execute the transition. You can switch to use a logical OR, which means only one of the conditions in the group needs to pass for the entire group to pass. This is a very useful feature as it allows you to combine multiple conditions to form a more complex logical unit.

For example, the User Is In Group condition lets you specify a single group, but with the AND operator, you can add multiple User Is In Group conditions to ensure the user must exist in all the specific groups to be able to execute the transition. If you use the OR operator, then the user will only need to belong to one of the listed groups. The only restriction to this is that you cannot use both operators for the same condition group.

*One transition can only have one condition group, and each condition group can only have one logical operator.*

## Adding a validator to transitions
Like conditions, transitions, by default, do not have any validators associated. This means transitions are completed as soon as they are executed. You can add validators to transitions to make sure that executions are only allowed to be completed when certain criteria are met. One good use case of validators is when a transition has a workflow screen to capture user input, and you need to validate the inputs, such as date format.

*Validators take place during a transition execution.*
Perform the following steps to add a validator to a transition:

1. Select the transition you want to add validators to.
1. Click on the Validators link.
1. Click on the Add validator link. This will bring you to the Add Validator To Transition page, which lists all the available validators you can add.
1. Select the validator you want to add.
1. Click on the Add button to add the validator.
1. Depending on the validator, you may be presented with the Add Parameters To Validator page where you can specify configuration options for the validator. For example, the Permissions validator will ask you to select the permission to validate against.

Similar to conditions, when there are multiple validators added to a transition, they form a validator group. Unlike conditions, you can only use a logical AND for the group. This means that in order to complete a transition, every validator added to the transition must pass its validation criteria. Transitions cannot selectively pass validations using a logical OR.

The following screenshot shows a validator being placed on the transition, validating if the user has entered a value for the Resolution Details field:
![](assets/markdown-img-paste-20201109201041436.png)

## Adding a post function to transitions
Transitions, by default, are created with several post functions. These post functions provide key services to Jira's internal operations, so they cannot be deleted from the transition. These post functions perform the following:

* Set the issue status to the linked status of the destination workflow step
* Add a comment to an issue if one is entered during a transition
* Update the change history for an issue and store the issue in the database
* Re-index an issue to keep indexes in sync with the database
* Fire an event that can be processed by the listeners
As you can see, these post functions provide some of the basic functions such as updating a search index and setting an issue's status after transition execution, which are essential in Jira. Therefore, instead of users having to manually add them in and risk the possibility of leaving one or more out, Jira adds them for you automatically when you create a new transition.

*Post functions take place after a transition has successfully executed.*

Perform the following steps to add a post function to a transition:

1. Select the transition you want to add post functions to.
1. Click on the Post Functions link.
1. Click on the Add post function link and select the post function you want to add.
1. Click on the Add button to add the post function.
1. Depending on the post function, you may be presented with the Add Parameters To Function page, where you can specify configuration options for the post function. The following screenshot shows an example from the Update Issue Field post function:
![](assets/markdown-img-paste-2020110920114984.png)

Just like conditions and validators, multiple post functions form a post function group in a transition. After a transition is executed, each post function in the group is executed sequentially as it appears in the list, from top to bottom. If any post function in the group encounters an error during processing, you will receive an error, and the remaining post functions will not be executed.

Since post functions are executed sequentially and some of them possess the abilities to modify values and perform other tasks, often, their sequence of execution becomes very important. For example, if you have a post function that changes the issue's assignee to the current user and another post function that updates an issue field's value with the issue's assignee, obviously the update assignee post function needs to occur first, so you need to make sure it is above the other post function.

You can move the positions of post functions up and down along the list by clicking on the Move Up and Move Down links. Note that not all post functions can be repositioned.
