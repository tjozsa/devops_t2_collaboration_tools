# Reporting in JIRA

[[_TOC_]]

## Using the Jira dashboard
The easiest way to display and share your project and sprint progress with everyone is to use the dashboard feature from Jira. A dashboard acts as a portal page for your project, and you can display different information about your project by adding gadgets onto the dashboard.

Jira comes with a number of gadgets that are designed specifically to display agile related information, including:

* **Sprint burndown gadget**: This displays a burndown chart of your sprint on the dashboard. The chart will be automatically updated to reflect the current data.
* **Days remaining in sprint gadget**: This displays how many days are left before the sprint is scheduled to be completed. It acts as a reminder for how much time is left on the clock.
* **Sprint health gadget**: This displays a bar chart showing information on how the sprint is progressing; for example, how much more work is still left to do.

The power of using the Jira dashboard is that you are not limited to using only the gadgets that are specifically designed for Agile. Jira comes with many other useful gadgets that you can use to drill down into your project and sprint. They are as follows:
* **Filter results gadget**: You can select a filter and display the result in a table. You can use this to display the most important issues in the sprint.
* **Two-dimensional filter statistics gadget**: This is similar to the filter results gadget, but instead of displaying a list of issues, it will display a statistical breakdown of the filter result based on the fields you choose.
* **Pie chart gadget**: You can select a filter and the result will be displayed as a pie chart, where each slice is based on a field of your choice; for example, priority.

As you can see, you can build a very useful dashboard by combining gadgets from both agile and non-agile based ones. You can even create your own gadget or download gadgets from third-party vendors to display information specific to your needs.

To create a dashboard for your project and sprint, perform the following steps:
1. Select the Manage Dashboards option from the Dashboards menu.
1. Click on the Create new dashboard button.
1. Enter a name for your new dashboard.
1. Select how you want to share the dashboard. Dashboards are private by default, so for others to see the dashboard, you must share the dashboard with them. As shown in the following screenshot, we are sharing the dashboard with members of the Top Secret Project.
1. Click on the Add button to create the dashboard:
![](assets/markdown-img-paste-20201109201621766.png)
*Make sure you click the Add button after you have selected who to share the dashboard with.*

Once you have created a new dashboard, you can start adding contents onto it with gadgets:

1. Click on the Add a new gadget link. It does not matter which one you click, as you can always reposition the gadgets after they have been added, by simply dragging them around on the dashboard.
1. Select the gadgets you want to add from the Add a gadget dialog by clicking on its Add gadget button.
1. Close the dialog once you have added all the gadgets you want:
![](assets/markdown-img-paste-20201109201651266.png)

Once you have added the gadgets, you will need to configure each gadget to display the data you want. For most gadgets, all you need to do is select the board, sprint, project, or filter to use:

![](assets/markdown-img-paste-20201109201703616.png)

You can also configure the layout of the dashboard. By default, the dashboard is divided into two columns of equal width. You can change that by clicking on the Edit Layout button and then selecting the layout you want.

## Using the wallboard
Another great Jira feature you can take advantage of is the wallboard. You can think of wallboards as Jira dashboards that you display on a big wall using a projector, or with a big monitor.

Using a wallboard is a great way to share information about your project with your team and other colleagues. The following screenshot shows an example of a wallboard. By taking the data out of an agile project, and projecting that onto a big screen, everyone will have instant and easy access to the information they need. As people walk by your team's work area, they will get a good idea of how the team is progressing:
![](assets/markdown-img-paste-20201109202002226.png)

To set up a wallboard for your agile project, you will first need to create a dashboard. Note that not all gadgets are wallboard-compatible, but all the agile gadgets that come out of the box are compatible. Once you have your dashboard ready, click on the Tools menu and select the View as Wallboard option, link it up to a big monitor, and you have yourself an awesome wallboard.

If you have multiple dashboards, you can create a slideshow of wallboards. All you have to do is select the Setup Wallboard Slideshow option from the Tools menu, as shown in the following screenshot, to set up the slideshow. Select the dashboards to include on the wall, and how you would like the slideshow to look like(Display Options), and use the View as Wallboard Slideshow option to display it:
![](assets/markdown-img-paste-20201109202024198.png)
