# Integrating Jira with Confluence
Confluence is a team collaboration solution from Atlassian (the maker of Jira) that enables teams to collaborate and create content together. Organizations often use it to create and share information related to projects, such as functional and design specifications. Jira works seamlessly with Confluence to provide you with a complete agile experience. In the following sections, we will look at how you can integrate Jira with Confluence to:

* Create epics and user stories with design documentation
* Manage and view your sprints on a calendar
Capture meeting notes for your sprint planning sessions
* Create retrospective reports at the end of each sprint
* Share and publish release information

[[_TOC_]]

## Setting up an application link with Confluence
If you have not already integrated Jira and Confluence together, you need to create a new application link. To create an application link with Confluence, perform the following steps:

Browse to the Jira administration console.
Select the Applications tab and then the Application links option.
Enter the URL to your Confluence instance and click on the Create new link button, as shown in the next screenshot:
1. Browse to the Jira administration console.
1. Select the Applications tab and then the Application links option.
1. Enter the URL to your Confluence instance and click on the Create new link button, as shown in the next screenshot:
![](assets/markdown-img-paste-20201110072935997.png)
1. Tick the The servers have the same set of users and usernames option if both Jira and Confluence share the same user repository; for example, LDAP.
>If you have a common user repository for both applications, such as LDAP, by enabling this option, users will have a seamless experience. Otherwise, they will be prompted to authorize access for the first-time viewing of contents from the other application.*

5. Tick the I am an administrator on both instances option if you have an administrator account on both Jira and Confluence. This will let you also create a reciprocal link from Confluence to Jira.
1. Click on the Continue button:
![](assets/markdown-img-paste-20201110073018633.png)
1. Verify that the onscreen information is correct. If both applications are able to communicate with each other successfully, it will display the URLs and application name and type, as shown in the following screenshot. Then, click on the Continue button:
![](assets/markdown-img-paste-2020111007311544.png)
1. Continue with the onscreen wizard, and once the application link is successfully created, you will see a success message and the new application link listed for Confluence:
![](assets/markdown-img-paste-20201110073127554.png)

## Creating Confluence pages from epics
Jira is a great tool to track and manage your day-to-day activities for your project, but it is not the best tool to capture detailed information for your tasks, such as design documentation and functional specifications for your epics. With Confluence as a documentation platform, there are a few ways you can create design documents and link them to your epics.

The first option is to create your documents called Confluence pages, directly from your agile board's backlog:

1. Browse to your agile board and go to its backlog.
1. Open up the Epics panel from the left-hand side.
1. Select and expand the epic you want to create a Confluence page for.
1. Click on the Links pages link from the epic.
1. Click on the Create page button from the dialog, as shown in the following screenshot:
![](assets/markdown-img-paste-20201110073148939.png)
1. After you click on the Create page button, you will be taken to Confluence in a new browser tab with the Create dialog displayed, as shown in the following screenshot. By default, the dialog will have the Project requirements template (also called a blueprint) preselected for you, but you can choose to use a different template if you want. Also note that the Select space field at the top will have the last Confluence space you have visited preselected, so make sure you select the correct space to create your new page in.
1. Click on the Create button after you have selected the correct space to create your new page in:
![](assets/markdown-img-paste-20201110073232765.png)
1. If this is the first time you have used the product requirements template, you will get a Let's get started information dialog, as seen in the following screenshot. Simply tick the Don't show this again option at the bottom and click on the Create button to create your new page:
![](assets/markdown-img-paste-20201110073249463.png)
1. Click on the Create button again to start working on the new page.

Confluence will present you with a new page and editor, with a predefined template. You can simply fill in the templates with information such as goals and requirements. A few important things to note:
* Make sure you give your page a title. A good practice is to name it after the linked epic.
* Reference the epic in Jira by clicking on the Link to related Jira epic of feature text label from the Epic field. This way, a reference link will be created between the requirement page and the epic issue.

With the page created, if you go back to Jira, the Linked pages link for the epic will change to 1 linked page, and clicking on that will show you the actual linked page, as shown in the following screenshot:

![](assets/markdown-img-paste-20201110073325102.png)

If you already have requirement pages created for the project, instead of creating new ones from the epics, you can simply link to those pages by clicking on the Link page button. After clicking on it, you will get a search box, and you can type in your page's title, find the page you want, and select it to create a link. This is illustrated in the next screenshot:

![](assets/markdown-img-paste-2020111007333879.png)

> If you cannot see the linked pages, make sure Confluence has the remote API enabled. See https://confluence.atlassian.com/x/vEsC for more details.

## Creating user stories from Confluence
If you are using the product requirements blueprint, as we have seen earlier, there is a section on the page for you to list out all the requirements for the feature. Once you have defined all the requirements with the team, you can create user stories directly on the page. To do this, perform the following steps:

1. Go to your product requirements page in Confluence.
1. Highlight the text of the requirement you want to create a user story with. The text you highlight will become the summary of the user story, as shown in the following screenshot:

![](assets/markdown-img-paste-20201110073416782.png)

3. Click the Jira icon. This will open up the Create Issue dialog, as shown in the following screenshot.
1. Make sure the project and issue type selection is correct. You can click on the Edit link to change that.
1. Enter a description for the user story.
1. If the Product Requirements page is already linked to an epic in Jira, you will see a Link to epic option. Uncheck this option if you do not want the user story to be added to the epic.
1. Click on the Create button to create the user story:
![](assets/markdown-img-paste-20201110073507294.png)

> If you have multiple requirements listed in the Requirements table on your page, you can click on the Create x issues from this table option at the bottom, and Confluence will automatically create a user story for each requirement you have.

After you have created the user stories, you will see a Jira issue added next to each of the requirements, showing their key and status. Their status will be automatically updated as the issues are being worked on. You will also see a Jira links button at the top of the page next to the breadcrumbs. Clicking on that will display all the Jira issues currently linked to this page, including any epics and user stories. Refer to the following screenshot:

![](assets/markdown-img-paste-2020111007352436.png)

>Currently, the Jira links button is only available if you use the default theme in Confluence.

## Planning your sprints with team calendars
As you and your team work on the sprint, it is often helpful to see how your sprints fit in with other activities your team might have: for example, if there are team members going on a vacation or with travel plans halfway through a sprint, or if there are other delivery commitments that might interfere with the sprint.

The key to solving this is to have all this information visually displayed on a single calendar, viewable and shared by the entire team, so everyone can stay well-informed, just like having tasks plotted on an agile board. To do this, perform the following steps:

1. Browse to your team's Team Calendar.
1. Click on the Add Event button.
1. Select the Jira Agile Sprints option for Event Type:

![](assets/markdown-img-paste-2020111007361234.png)

4. Select the project that belongs to your Scrum board.
1. Enter a name for the event.
1. Click on the OK button to create the event.

> Team Calendars for Confluence is a separate product you can get for Confluence from Atlassian Marketplace at https://marketplace.atlassian.com/plugins/com.atlassian.confluence.extra.team-calendars/cloud/overview

Once you have created the event, Team Calendar will get all the sprints you have for the selected project, and display them on the calendar. As shown in the following screenshot, we have two sprints, Sprint 2 and Sprint 3. We can also see that Tom Johnson will be away at the start of Sprint 2, which might have an impact on the team's ability to complete everything in the sprint on time. Also, if you have all team members' vacation plans on the calendar, then during your sprint planning sessions, you will have all the information you need when deciding how much work should go into the sprint and how long the sprint should be:

![](assets/markdown-img-paste-20201110073655334.png)

With the calendar all set up, you can also share and embed it. A good way to use this feature is to create a new Confluence page in the same project space where you have all your requirements documentation, call it something like Project Calendar, and then embed the calendar into the page. To embed the calendar into a page:

1. Click on the Create button at the top of the page.
1. Select the project space for the Select space field.
1. Now select the Blank page option and click on the Create button.
1. Name the page Project Calendar.
1. Select and add the Team Calendar macro into the page.
1. Click on the Add Existing Calendar option.
1. Search for the calendar you have created, and click on the Add button.

![](assets/markdown-img-paste-20201110073725976.png)

After you have created the page, you will have all the information about the project in a single place along with the Confluence space for the project, for easy access. One additional step you can take is to create a quick shortcut link on your Scrum board to the Project Calendar page, so it is just a click away when you need it.

To create a link to the page:

1. Browse to your Scrum board.
1. Click on the Add link option from the left-hand side.
1. Enter the URL for the Project Calendar page.
1. Enter a label for the link; that is, Project Calendar.
1. Click on the Add button to create the link.

The link will be displayed under the Project Shortcuts section on the left-hand side, as shown in the following screenshot. So during your sprint planning sessions, or work sessions on the active sprint, you can easily access Project Calendar and get the most up-to-date information:

![](assets/markdown-img-paste-20201110073801144.png)

## Capturing sprint meeting notes
As we have seen, you can plan and visualize your sprints with Confluence's Team Calendar. Another important part of your sprint planning session is to keep records of the meetings, capturing what was discussed, decisions made, and being able to reference back to those meeting notes in the context of your sprints.

Just like requirement documents, Confluence is also a great place to capture and store this information. From inside your Scrum board, you can create and link each sprint to pages in Confluence, just like with epics. To create a meeting note for a sprint:

1. Browse to your Scrum board.
1. Click on Backlog on the left-hand side to display all your sprints.
1. Click on the Linked pages link for the sprint that you want to create a meeting note for.
1. Click on the Create page button if you want to create a new meeting note page, or the Link page button if you already have the meeting note ready:

![](assets/markdown-img-paste-20201110073930403.png)

If you click on the Create page button, you will be taken to Confluence, with the Create dialog showing. It will have the Meeting notes blueprint preselected. Make sure the correct space is selected and click on the Create button; you will be able to start entering your meeting information. Once you have created and saved the meeting note, the page will have a Jira link referencing the sprint it has been created for, as shown in the following screenshot, and the sprint will also list all the meeting notes it has:

![](assets/markdown-img-paste-20201110073942861.png)

## Creating retrospective reports
Other than creating meeting notes, another great feature is being able to create retrospective sprint reports at the end of each sprint. Remember, one of the key ideas behind agile is continuous improvement, so it is important that at the end of each sprint, the entire team comes together and discusses what the they did well, and what went wrong during the sprint; and also to summarize lessons learned and discuss how to improve the process in the next sprint as a team.

To create a retrospective report for your sprint, perform the following steps:

1. Browse to your Scrum board.
1. Click on Reports from the left-hand side.
1. Select the sprint to be reported on and select Sprint Report.
1. Click on the Linked pages link.
1. Click on the Create page button:

![](assets/markdown-img-paste-20201110074014205.png)

1. Click on the Next button in the Confluence Create dialog.
1. Enter a title for the report or leave the default.
1. Add all the team members present in the retrospective meeting.
1. Click on the Create button to start work on the report.

Just like all other reports created, a reference link will be created between the report and the sprint, so you can easily go back and forth between the two.

## Displaying your project in Confluence
The last integration feature between Jira and Confluence is to create reports on the project based on specific versions. There are two types of reports you can create:

* **Change log report**: The change log report lists out all the issues that are part of a selected version. This saves you the hassle of manually compiling a list of issues and entering them. This is a great way to communicate changes within a given version to your customers and other stakeholders.
* **Status report**: The status report is a live report that shows the status of the project in a number of pie charts.
To create these reports, you will start in Confluence instead of Jira:

1. Log into Confluence and browse to your project space.
1. Click on the Create button at the top or press the C key on your keyboard.
1. Select Jira Report from the Create dialog and click on Create.
1. Choose the report you want to create and click the Next button. In our example, we are creating a status report:

![](assets/markdown-img-paste-20201110074113427.png)

5. Select the project and version to report on. These fields are autopopulated based on data coming from Jira.
1. Enter a title for the report.
1. Click on the Create button to start work on the report.

The default report template will be auto-populated based on the information you have provided, so you can simply click on the Save button to create the report without any further changes. The following screenshot shows a default status report:

![](assets/markdown-img-paste-20201110074134302.png)
