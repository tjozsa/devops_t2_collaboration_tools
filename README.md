# Introduction to Development
This repository contains the complete workshop of DevOps Collaboration Tools module.
It is advised to follow the content as indicated by the index number in the folder name.
For example content of folder staring with `009_...` should be processed before content 
folder starting with `011_...`.

## Theory and Demos
These folders contain portions of the theoratical matherial and demonstrations.

## Homework
Should there be matherial left that we could not cover during the workshop days, it will be announced as homework.

## Recordings
You will have access to the recording of the workshop activities. These recordings will be shared with you later on this page.

## Knowledge Check Quiz
You will receive a link during the week of the workshops to participate in a knowdeldge check quiz.
Please participage to get feedback about how well you learned the material of this week.

## Prerequisites
You should have 
* GitLab account
* Attlassian accouont (for Jira cloud, Confluence cloud and Trello cloud exercises)

## Tools used throughout the workshop demos, and homework assingments
* None, if any will be announced later.