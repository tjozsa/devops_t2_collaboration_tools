# Basics Of JIRA Tool
JIRA in its entirety is based on 3 concepts.

![](assets/markdown-img-paste-20201109123754444.png)

* **Issue**: Every task, bug, enhancement request; basically anything to be created and tracked is considered an Issue.
* **Project**: A collection of issues
* **Workflow**: A workflow is simply the series of steps an issue goes through starting from creation to completion.

Say the issue first gets created, goes to being worked on and when complete gets closed. The workflow in this case is:

![](assets/markdown-img-paste-20201109123821120.png)

## The agile board

The agile board, or simply board, is a new user interface introduced in JIRA Agile. It is the main interface you, as the end user, will be using most of the time. If we use a real life comparison, the agile board will be your white board where you will place your user stories as post-it notes, which will be represented as cards. Essentially, you get the advantages of being able to visualize your backlog, as well as the added benefits of keeping track of changes and the progress of your tasks, along with reporting capabilities. For many teams, to keep using a white board is still very valuable, and we will look at ways we can combine both the white board and the JIRA Agile board in Chapter 5, JIRA Agile – Advanced.

In JIRA Agile, there are two different types of boards, one for Scrum and the other for Kanban, each with their own features. There is also a classic board, which is no longer under active development, so we will not be covering it. The following screenshot shows a sample Scrum board in the work mode:

![](assets/markdown-img-paste-20201108153128844.png)

## Card
A card is like a post-it note you would have on your white board. It captures the user story and represents the requirement or feature that is to be implemented. In JIRA Agile, each card is an issue in JIRA. The following screenshot shows what a card looks like in JIRA Agile:

![](assets/markdown-img-paste-20201108153150954.png)


## Issues and issue types
Every unit of work in JIRA Agile, such as a story or an epic, is an issue in JIRA. They are simply new issue types created by JIRA Agile when it is installed. The following new issue types will be added:

* **Epic**: This represents a big user story that has not been broken down into finer-grained requirements. In JIRA Agile, epics are usually used to define the "theme" for several stories that will be part of it, as well as modules or major components in a big development project.
* **Story**: This represents a single feature to be implemented. It is usually used to capture requirements from the end user's perspective. For this reason, stories are often written in non-technical language while focusing on the desired results of the feature.
* **Technical task**: This is a subtask issue type that represents the actual technical work that needs to be done in order to implement the story.

## Filters and JQL

JIRA Agile is able to work on either one specific project, or multiple projects at once. When you want to have multiple projects, you will need to use filters to define what issues will be included. For this reason, understanding and being able to use JIRA Query Language (JQL) effectively can be very handy. You can find more information on JQL at https://confluence.atlassian.com/display/JIRA/Advanced+Searching.

## Workflows

Workflow is the heart of JIRA and is what powers JIRA Agile in the background. As we will see in later chapters, JIRA Agile is able to integrate with your existing workflows, or adapt to and model after your development process. When you are just getting started, you don't have to know much about workflow as JIRA Agile will take care of it for you.
