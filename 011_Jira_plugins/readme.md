# Jira Apps(Plugins)
Jira comes feature packed already (batteries included if you will). Nevertheless there is a rich marketplace for Jira apps (called plugins on-premise).

For instance you might need a tool to easily track definition of done and acceptance criteria for your User Stories.

You can go with [Smart Checklist for Jira](https://marketplace.atlassian.com/apps/1215277/smart-checklist-for-jira-free?hosting=cloud&tab=overview)

Other plugins can be found on https://marketplace.atlassian.com
