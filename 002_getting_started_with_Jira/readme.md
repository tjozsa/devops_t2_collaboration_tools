# You need an Atlassian account
[Start here by signing up for one!](https://id.atlassian.com/signup)

![](assets/markdown-img-paste-20201109071054967.png)

# User Trial
![](assets/markdown-img-paste-20201109071143291.png)

# Let's start with Jira trivial
![](assets/markdown-img-paste-20201109071221604.png)

![](assets/markdown-img-paste-20201109071240373.png)

![](assets/markdown-img-paste-20201109071302175.png)

# Answer some questions or skip theme
Atlassian wants to know some info about you and your project, but you can skip it.
Then your project is being set up:

![](assets/markdown-img-paste-20201109071426678.png)

# You can start inviting people right away
![](assets/markdown-img-paste-2020110907150162.png)

# You are greeted with project suggestion wizard, but you can skip this
![](assets/markdown-img-paste-20201109074026153.png)

My results:

![](assets/markdown-img-paste-20201109074046477.png)

# Choose one just to get started.

I choose Scrum

![](assets/markdown-img-paste-20201109074143368.png)

# Jira Cloud is Constantly Evolving

New Issue View is currently the "New Kid on the block"

![](assets/markdown-img-paste-20201109074238863.png)

![](assets/markdown-img-paste-20201109074343376.png)

# You are ready to roll.

![](assets/markdown-img-paste-20201109074421899.png)
