# Connect Trello to Jira with this power-up

[Trello Jira Power-Up](https://trello.com/power-ups/586be36326cc4c7e9f70beb3/jira)

Connect Jira and Trello to help all your teams work better together. The Jira Power-up allows you to...

* Link Jira issues to cards, connecting different teams across your organization.
* View important issue information on the detailed card view.
* See if your linked issues are done at glance (from the front of the card)
* Automatically links the Jira issue back to a Trello card so everyone in your team knows how something came to be.
* Quickly jump to your Jira project by creating a link on your board.
