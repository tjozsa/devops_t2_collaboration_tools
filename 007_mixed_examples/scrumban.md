## Scrumban
The first mixed agile methodology is called **Scrumban**. As you may have guessed from its name, it is a methodology based on *Scrum*, but with a *hint of Kanban* mixed in.

The Scrum methodology revolves around grooming a backlog of tasks and running your project in iterations, called sprints. While this is a great way to plan and manage your project, it sometimes fall short when it comes to actually visualizing the execution of your sprints, which is something Kanban is great at.

So instead of forcing you to make a choice between the two methodologies, Jira lets you use Scrum as the basis, so you can still run your project with time-boxed sprints and bring in some of the features and benefits of Kanban, and this hybrid methodology is called Scrumban.

In Kanban we introduced column constraints and how Jira uses that to visually show you bottlenecks in your workflow on a Kanban board. This same approach is also how Jira lets you use part of Kanban's features in Scrum.

For example, we have set a constraint that there should be a minimum of three and no more than five issues in the In Progress column for our Scrum board, as shown in the following screenshot:

![](assets/markdown-img-paste-20201109195037716.png)

Just like with Kanban, once you have placed constraints on a column, if it is violated, the column will be highlighted. As shown in the following screenshot, the In Progress column has a maximum constraint of three issues, but we have four, so it is highlighted in red. The In Review column has a minimum constraint of two issues, but we have one, so it is highlighted in yellow:

![](assets/markdown-img-paste-2020110919505970.png)

Column constraints do not prevent you from violating the limits. They simply help to flag areas where there might be a problem. Setting the limits for your columns can be tricky, especially when you are just getting started. You should start with your gut feeling, run a few sprints, and refine the limits as you and your team get a better feel for the workflow. One way to get started is to base your limits on the number of people you have on your team. For example, if you have five developers, then your maximum limit for your In Progress columns (assuming this means development) should be no more than five, as it is not logical to have five people working on six issues at the same time.

So, if we look at the example in the preceding screenshot, we are over the maximum limit for the In Progress column. This could mean that the team, especially the developers, has overcommitted on their tasks; someone might have decided to work on two tasks in parallel. This is causing a bottleneck, and issues are not being completed quickly enough to move to the In Review column, causing a minimum limit violation, where you have reviewers waiting around for work. This data can be very useful in your sprint retrospective meetings to help you review the problem and refine the process.

So, as you can see, setting column constraints is situational, and is based on your team's composition as well as their abilities. As things change, you will need to change your limits accordingly. Remember, the goal here is to measure, identify, and improve.
