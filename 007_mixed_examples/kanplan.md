# Kanplan
The second mixed agile methodology is called Kanplan. As the name suggests, this methodology leans more toward Kanban than Scrum. In a nutshell, Kanplan allows you to have a more robust backlog, while still allowing you time to enjoy the flexibility of Kanban.

As we have seen in the previous chapter, by default, a Kanban board does not have a true backlog. You will usually use the first column on the board as the backlog. While this approach works when you are just getting started, very soon it will become very hard to manage as the number of items in the column grows. This makes it difficult to get a good grasp on all the outstanding issues, and also causes trouble when your team tries to prioritize things to work on, especially if they have also defined custom swimlanes for your board.

To enable backlog for a Kanban board, follow these steps:

1. Navigate to the Kanban board that you want to add a backlog for.
1. Click on the Board drop-down menu and select the Configure option.
1. Select the Columns option from the left-hand panel.
1. Drag one or more workflow statuses into the Kanban backlog section on the left:

![](assets/markdown-img-paste-20201109195403453.png)

As shown in the preceding screenshot, we have a Backlog column in our Kanban columns setup, but since we now want to use the backlog feature, we move the Backlog status to the Kanban backlog column instead.

The new Kanban backlog works just like any board columns: You can have more than one status mapped to it, so any issues in one of the mapped statuses will appear in the new backlog. Note that because of this, any issues that are not in a mapped status will not appear in the new backlog, so make sure you add all the appropriate statuses.

> Simply mapping all your existing statuses in the default Backlog column to the new Kanban backlog would be the easiest way to ensure you do not leave any issues off.

Once you have enabled backlog for your Kanban board, you will see the new Backlog option at the top of the left-hand navigation panel when you go to your project. Clicking on that will show you a very similar interface to Scrum, as
shown in the following screenshot:

![](assets/markdown-img-paste-20201109195459889.png)

Just like a Scrum' backlog, all issues are ordered from top to bottom. You can drag any issue up and down the backlog queue to prioritize it. For issues that are at the very bottom, you can right-click on the issue and send it straight to the top.

One key difference between a Kanban backlog and a Scrum backlog is that since Kanban does not have sprints, you do not create them and then add issues to the sprints. Instead, you add issues to the next status or column in the workflow. In the example shown in the preceding screenshot, this is the Selected for Development column. If you are familiar with Scrum, that is the section where your new sprint is, and you will be adding issues into the sprint. Here in Kanban, you are prioritizing and moving issues from the backlog's to-do list into an action list. With this change, your new Kanban board will look something similar to the one shown in the following screenshot, where the board will focus on the actual issues that have been prioritized without the noise of a big overwhelming Backlog column to distract your team with:

![](assets/markdown-img-paste-20201109195518772.png)

And this is not where the new Kanplan backlog stops. By leveraging the same backlog feature available to Scrum, you now also get access to features such as managing your issues with epics in a visual way, just like in Scrum. This is especially helpful for Scrum teams that are just starting to move toward using Kanplan. The epics panel for your backlog should be enabled by default, but if for some reason it is not, you can always manually enable it by going to the Column management page of your agile board's configuration panel:

![](assets/markdown-img-paste-20201109195529556.png)

