# Navigating issues

[[_TOC_]]

## Issue navigator
The issue navigator is the primary interface through which you will be performing all of your searches in Jira. You can access it by clicking on the Issues menu in the top menu bar and then selecting Search for issues.

The issue navigator is divided into several sections. The first section is where you will specify all of your search criteria, such as the project you want to search in and the issue type you are interested in. The second section displays the results of your search. The third section includes the operations that you can perform on the search results, such as exporting them into various formats. The fourth and last section lists a number pre-configured, and user-created filters.

When you access the issue navigator for the first time, you will be in the basic search mode. The following screenshot shows the issue navigator in this mode:

![](assets/markdown-img-paste-20201109203306882.png)

In basic search, as you can see, you specify your search criteria with UI controls, selecting the values for each field.

If you previously visited the issue navigator and chose to use a different search option, such as advanced search, then Jira will remember this and open up advanced search instead.

## Basic search
The basic search interface lets you select the fields you want to search with, such as issue status, and specify the values for these fields.

With basic search, Jira will prompt you for the possible search values for the selected field. This is very handy for fields, such as status, and select list-based custom fields, so you do not have to remember all the possible options.

For example, as shown in the following screenshot, we are searching for issues in the Demonstration Project, with the status as Open. And for the status field, Jira will list all the available statuses:

![](assets/markdown-img-paste-20201109202518743.png)

While working with the basic search interface, Jira will have the default fields of project, issue type, status, and assignee visible. You can add additional fields to the search by clicking on the More drop-down option and then selecting the field you want to use in the search. Perform the following steps to construct and run a basic search:

1. Browse to the Issue Navigator page. If you do not see the basic search interface, and the Basic link is showing, click on it to switch to basic search.
1. Select and fill in the fields in the basic search interface. You can click on More to add more fields to the search criteria.

Jira will automatically update the search results every time you make a change to the search criteria.

> When working with basic search, one thing to keep in mind is that the project and issue type, context of the custom fields are taken into consideration. (Please see Chapter 5, Field Management, for field configuration). If a custom field is set to be applicable to only specific projects and/or issue types, you have to select the project and issue type as part of your search for the custom field to show up.

## Advanced search with JQL
Basic search is useful and will fulfill most of the user's search needs. However, there are still some limitations. One such limitation is that basic search allows you to perform searches based on inclusive logic but not exclusive logic. For example, if you need to search for issues in all but one project, you will have to select every project except for the one to be excluded since the basic search interface does not let you specify exclusions.

This is where advanced search comes in. With advanced search, instead of using a field selection-based interface, you will be using what is known as the **Jira Query Language (JQL)**.

JQL is a custom query language developed by Atlassian. If you are familiar with the **Structured Query Language (SQL)** used by databases such as MySQL, you will notice that JQL has a similar syntax; however, JQL is not the same as SQL.

One of the most notable differences between JQL and SQL is that JQL does not start with a select statement. A JQL query consists of a field followed by an operator, and then by a value such as assignee = john or a function (which will return a value) such as assignee = currentUser().

You cannot specify what fields to return from a query with JQL, which is different from SQL. You can think of a JQL query as the part that comes after the where keyword in a normal SQL select statement. The following table summarizes the components in JQL:

| JQL component |	Description |

| Keyword | Keywords in JQL are special reserved words that do the following:
* Join queries together, such as AND
* Determine the logic of the query, such as NOT
* Have special meaning, such as NULL
* Provide specific functions, such as ORDERBY |
| Operator	| Operators are symbols or words that can be used to evaluate the value of a field on the left and the values to be checked on the right. Examples include the following:
* Equals: =
* Greater than: >
* IN: When checking whether the field value is in one of the many values specified in parentheses |
| Field | Fields are Jira system and custom fields. When used in JQL, the value of the field for issues is used to evaluate the query.|
| Functions |	Functions in JQL perform specific calculations or logic and return the results as values that can be used for evaluation with an operator.|

Each JQL query is essentially made up of one or more components. A basic JQL query consists of the following three elements:

* Field: This can be an issue field (for example, a status) or a custom field.
* Operator: This defines the comparison logic (for example, = or >) that must be fulfilled for an issue to be returned in the result.
* Value: This is what the field will be compared to; it can be a literal value expressed as text (for example, Bug) or a function that will return a value. If the value you are searching for contains spaces, you need to put quotes around it, for example, issuetype = "New Feature".

Queries can then be linked together to form a more complex query with keywords such as logical AND or OR. For example, a basic query to get all the issues with a status of Resolved will look similar to this:

```
status = Resolved
```
A more complex query to get all the issues with a Resolved status, a Bug issue type, and which are assigned to the currently logged-in user, will look similar to the following (where currentUser() is a JQL function):
```
issuetype = Bug and status = Resolved and assignee = currentUser()
```
Discussing each and every JQL function and operator is out of the scope of the book, but you can get a full reference by clicking on the Syntax Help link in the advanced search interface. The full JQL syntax reference can be found at https://confluence.atlassian.com/x/ghGyCg.

You can access the advanced search interface from the Issue Navigator page, as follows:

1. Browse to the Issue Navigator page
1. Click on the Advanced link on the right
1. Construct your JQL query
1. Click on the Search button or press the Enter key on your keyboard

As JQL has a complex structure and it takes some time to get familiar with, the advanced search interface has some very useful features to help you construct your query. The interface has an autocomplete feature (which can be turned off from the General Configuration setting) that can help you pick out keywords, values, and operators to use.

It also validates your query in real time and informs you whether your query is valid, as shown in the following screenshot:

![](assets/markdown-img-paste-20201109203151921.png)

If there are no syntax errors with your JQL query, Jira will display the results in a table below the JQL input box.

You can switch between basic and advanced search by clicking on the Basic/Advanced link while running your queries, and Jira will automatically convert your search criteria into and from JQL. In fact, this is a rather useful feature and can help you learn the basic syntax of JQL when you are first starting up, by first constructing your search in basic and then seeing what the equivalent JQL is.

>Switching between simple and advanced search can help you get familiar with the basics of JQL.

You need to take note, however, that not all JQLs can be converted into basic search since you can do a lot more with JQL than with the basic search interface. The Basic/Advanced toggle link will be disabled if the current JQL cannot be converted to the basic search interface.

## More about advanced searching
[Advanced searching](https://confluence.atlassian.com/jiracoreserver075/advanced-searching-935563511.html)

## Using quick filters
When you have a big project team, your board can get very busy. Sometimes you want to narrow them down and focus on issues that fit specific criteria, such as bugs, or issues that are assigned to a specific user.

By using quick filters, we can remove the unnecessary noise by filtering out all the issues that do not fit the criteria, letting you focus on the issues that you care about, as shown in the following screenshot:

![](assets/markdown-img-paste-20201109202140816.png)

You can think of quick filters as additional views for your Scrum board. For example, the preceding screenshot shows four quick filters for the Scrum board: Stories Only, Bugs Only, Only My Issues, and Recently Updated. By using the Bugs Only quick filter, you can get a view of your board showing only bug issues. You can toggle a quick filter on and off by clicking on it. When turned on, it will be highlighted in blue and update the board with only issues that fit the filter's criteria. Clicking on the filter again will unapply it. You can have more than one quick filter applied to a board at the same time, and only issues that fit all the filters' criteria will be shown.

## Creating new quick filters
All agile boards come with two default quick filters:
* Only My Issues: This displays issues that are assigned to the currently logged-in user
* Recently Updated: This displays issues that have been updated in the last 24 hours

If you are the board administrator, you can create new quick filters for your board to help you and your team to better visualize your issues. To do this, perform the following steps:

1. Navigate to the agile board that you want to customize.
1. Click on the Board drop-down menu and select the Configure option.
1. Select Quick Filters from the left-hand navigation menu.
1. Enter a name for the filter in the Name field. The name entered here will be displayed on the agile board.
1. Enter the search query for the filter in the JQL field, as shown in the following screenshot.
1. Click on the Add button to create the new filter.

Once created, the new quick filter will be available to everyone using the board:

![](assets/markdown-img-paste-20201109202300967.png)

In our example, as shown in the preceding screenshot, the JQL query issuetype = Bug means that issues that have the value Bug for their issue type field will be included.
